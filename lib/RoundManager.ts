import { getLogger } from './Logger';
import { PlayerRound, Point, Challenge } from './Types';
import { RoundScorer } from './Scoring';
import { PlayerManager } from './PlayerManager';
import { ChallengeManager } from './ChallengeManager';
import { Botter } from './Botter';

const log = getLogger('RoundManager');

export class RoundManager {
    private roundScorer;
    private playerManager;
    private challengeManager;

    constructor (private db) {
        this.roundScorer = new RoundScorer();
        this.playerManager = new PlayerManager(this.db);
        this.challengeManager = new ChallengeManager(this.db);
    }

    async getRoundsInfoForPlayer(playerId, challengeId) {
        return await this.db.prepare(`SELECT * from playerRounds 
            where playerId = ? and challengeId = ? order by roundId asc`)
            .all(playerId, challengeId) as PlayerRound[];
    }

    async getRoundsInfo(challengeId) {
        return await this.db.prepare(`SELECT * from playerRounds 
            where challengeId = ? order by roundId asc`)
            .all(challengeId) as PlayerRound[];
    }

    async insertRoundScore(roundScore:  PlayerRound) {
        return await this.db.prepare(`insert into playerRounds 
            (roundId, challengeId, playerId, score, lon, lat, distance) values (?, ?, ?, ?, ?, ?, ?)`)
            .run(roundScore.roundId, roundScore.challengeId, roundScore.playerId, roundScore.score,
            roundScore.lon, roundScore.lat, roundScore.distance);
    }

    async getPointsForChallenge(challengeId) {
        const pointIds = await this.db.prepare(`SELECT points from challenges 
            where geoId = ?`).all(challengeId);

        if (pointIds.length == 0) return pointIds;
        if (pointIds) {
            return await this.db.prepare(`SELECT * from points 
                where id in (?, ?, ?, ?, ?) order by id asc`)
                .all(pointIds[0].points.split(',')) as Point[];
        }
    }
    
    async postResult(roundId, challengeId, playerId, lat, lon) {
        try {
            // TODO: restrictions:
            // check if exists / is open challengeId
            // check if exists / is open roundId
            // check if exists playerId
            // check if there is a gap in rounds!

            if (!roundId || !challengeId || !playerId || !lat || !lon) return "not set error";

            const challenge = await this.challengeManager.getChallenge(challengeId) as Challenge
            const playerRound = {
                roundId, challengeId, playerId, lat, lon, type: challenge.type
            } as PlayerRound;

            const points = await this.getPointsForChallenge(challengeId);
            const scoredRound = await this.roundScorer.scoreRound(points[roundId-1], playerRound);
            log.info(`Scored a round!: ${scoredRound}`);

            await this.insertRoundScore(scoredRound);

            if (points.length == roundId) {
                // hack
                const realChallengeId = await this.db.prepare(`SELECT id from challenges 
                    where geoId = ?`).get(challengeId).id;

                // finalize scoring
                const rounds = await this.getRoundsInfoForPlayer(playerId, challengeId);
                const totalScore = rounds.reduce((res, round) => res + round.score, 0 );
                const totalDistance = rounds.reduce((res, round) => res + round.distance, 0 );
                const place = await this.playerManager.insertScore(realChallengeId, playerId, challengeId, totalScore);
                const name = (await this.playerManager.getPlayerById(playerId)).tName;

                let msg;
                switch (place) {
                    case 0:
                        msg = `[${challengeId}](http://lckp.eu/${challengeId}) - ${name} took the 1st place! 🔥`;
                        break;
                    case 1:
                        msg = `[${challengeId}](http://lckp.eu/${challengeId}) - ${name} got into 2nd! 🏇`
                        break;
                    case 2:
                        msg = `[${challengeId}](http://lckp.eu/${challengeId}) - ${name} is now 3rd! 🐄`
                        break;
                    default:
                        msg = `[${challengeId}](http://lckp.eu/${challengeId}) - Thanks for playing, ${name}!`
                        break;
                }

                Botter.sendMessage(msg);
            }

            return scoredRound;
        } catch (err) { 
            log.error(err, 'Error inserting a round result');
        }   
    }
}
