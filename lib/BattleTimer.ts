import { broadcast } from "./sockets/server";

const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

export class BattleTimer {
    private id

    constructor (id: string) {
        this.id = id
    }

    async runTimer(resetTime: number = 20, currentTime: number = 0) {
        await broadcast('time', resetTime - currentTime)

        if (currentTime === resetTime) {
            console.log('Timer is done!')
            return
        }

        await sleep(1000)

        this.runTimer(resetTime, currentTime + 1)
    }
}