const Database = require('better-sqlite3');
const inside = require('point-in-geopolygon');
const cnt = require('../resources/countries.json');
const writeJsonFile = require('write-json-file');
const loadJsonFile = require('load-json-file');

import { Point, PlayerRound, Challenge, Stats } from './Types';
import { getLogger } from './Logger';
const log = getLogger("Stats");

export const fetchStats = async (country: string, id: number) => {
    try {
        const stats = await loadJsonFile('./stats.json');
        const countries = Object.keys(stats);
        const foundCountry = countries.find(cnt => cnt.toLowerCase().replace( /\s/g, '').startsWith(country.toLowerCase().replace( /\s/g, '')));
        if (!foundCountry) return {"msg": "Could not find such a country!"}
        const playerStats = stats[foundCountry].find(player => player.player == id)
        if (!playerStats) return {"msg": "Could not find your stats for this country!"}

        return playerStats as Stats;
    } catch (e) {
        log.error(e);
        return {"msg": "no..."};
    }
}

export const topStatsPerPlayer = async (tId: string) => {
    const stats = await loadJsonFile('./stats.json');
    const countries = Object.keys(stats);

    const rankCountries = []
    for (const country of countries) {
        
        const countryObj = stats[country].find(c => c.player == tId)
        if (!countryObj) continue
        
        rankCountries.push(countryObj)
    }

    rankCountries.sort((a, b) => (b.distanceRank > a.distanceRank) ? -1 : 1)
    const distanceRank = rankCountries.slice(0, 5)

    rankCountries.sort((a, b) => (b.hitRank > a.hitRank) ? -1 : 1)
    const hitRank = rankCountries.slice(0, 5)

    rankCountries.sort((a, b) => (b.pointsRank > a.pointsRank) ? -1 : 1)
    const pointsRank = rankCountries.slice(0, 5)
    
    return {
        distanceRank,
        hitRank,
        pointsRank
    }
}
export const generateStats = async (db) => {
    log.info('Calculating stats...');

    try { 
        const pts = await db.prepare("select * from points order by country desc").all() as Point[];
        const rounds = await db.prepare("select * from playerRounds").all() as PlayerRound[];
        const challenges = await db.prepare("select * from challenges").all() as Challenge[];

        const stats = [];
    
        for (const chall of challenges) {
            if (chall.points) {
                const points = chall.points.split(",");
                for (let i = 0; i < points.length;i++) {
                    const filteredRounds = rounds.filter(round => parseInt(round.roundId) === i + 1)
                        .filter(round => round.challengeId == chall.geoId);
                    const point = pts.find(pt => parseInt(pt.id) === parseInt(points[i]));

                    for (const filteredRound of filteredRounds) {
                        const loc = [filteredRound.lon, filteredRound.lat];
                        const res = inside.feature(cnt, loc);
                        let oneStat = {};

                        oneStat = {
                            country : point.country,
                            distance: filteredRound.distance,
                            score: filteredRound.score,
                            playerId: filteredRound.playerId,
                        }

                        if (res == -1) {
                            oneStat["hit"] = false;
                        } else {
                            oneStat["hit"] = point.country == res["properties"]["ADMIN"]
                        }

                        stats.push(oneStat);
                    }
                }
            }
        }

        const players = Array.from(new Set(stats.map(stat => stat.playerId)));
        const countries = Array.from(new Set(stats.map(stat => stat.country)));
        
        
        const fullStats = [];

        let countryProcessed = {};
        for (const country of countries) {
            countryProcessed[country] = [];

            for (const player of players) {
                const playerstats = stats.filter(stat => stat.playerId == player);
                const countryStats = playerstats.filter(stat => stat.country == country);
                if (countryStats.length === 0) continue;
                const hit = Math.round(countryStats.filter(stat => stat.hit == true).length / countryStats.length * 100);
                const distance = Math.round(countryStats.reduce((accum,item) => accum + item.distance, 0) / countryStats.length);
                const points = Math.round(countryStats.reduce((accum,item) => accum + item.score, 0) / countryStats.length);
                
                countryProcessed[country].push({
                    player,
                    country,
                    hit,
                    distance,
                    points
                })
            }
        }

        for (const cnt in countryProcessed) {
            const processedCountry = countryProcessed[cnt];

            processedCountry.sort((a, b) => (b.hit > a.hit) ? 1 : -1);

            for (let i = 0; i < processedCountry.length; i++) {
                processedCountry[i]['hitRank'] = i+1;
            }

            processedCountry.sort((a, b) => (b.points > a.points) ? 1 : -1);

            for (let i = 0; i < processedCountry.length; i++) {
                processedCountry[i]['pointsRank'] = i+1;
            }

            processedCountry.sort((a, b) => (a.distance > b.distance) ? 1 : -1);

            for (let i = 0; i < processedCountry.length; i++) {
                processedCountry[i]['distanceRank'] = i+1;
            }
        }
        fullStats.push(countryProcessed);

        (async () => {
            await writeJsonFile('./stats.json', countryProcessed, {sortKeys: true});
        })();
        
    } catch (e) {
        console.log(e)
    }
}

topStatsPerPlayer('9')