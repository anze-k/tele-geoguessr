
import * as socketIO from 'socket.io'
import { BattleTimer } from '../BattleTimer';
import { submitResult } from '../Battles';
import { Point, PlayerRound } from '../Types';


interface ServerToClientEvents {
    userDisconnected: (a: string) => void;
    newUser: (d: unknown[]) => void;
    submittedResult:  (socket: UserResultData) => void;
    resultReceived:  (socket: UserResultData) => void;
    sendPowerup: (socket: PowerUpUse)  => void;
    sendMessageToRoom: (socket: Message) => void;
  }
  
  interface ClientToServerEvents {
    newUser: (data: string) => void;
    disconnect: (data: string) => void;
    locationSubmitted: (socket: UserSocketData) => void;
    usePowerup: (socket: PowerUpUse) => void;
    sendMessage: (socket: Message) => void;
  }
  
  interface InterServerEvents {
    ping: () => void;
  }
  
  interface SocketData {
    name: string;
    age: number;
  }

  interface UserSocketData extends socketIO.Socket<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData> {
      userId: string
  }

  interface UserId {
    userId: string
  }

  interface UserResultData extends UserId {
    point: Point
    score: PlayerRound
  }

  interface PowerUpUse extends UserId {
    powerupId: string
  }

  interface Message extends UserId {
    msg: string
  }

const activeUsers = new Set();


export function broadcast(event: string, data: any) {
    io.emit(event, data)
}

const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: '*',
  }
})



io.on("connection", async function (socket: UserSocketData) {
  socket.on('newUser', function (data) {
    console.log(data)
    socket.userId = data;
    activeUsers.add(data);
    io.emit('newUser', [...Array.from(activeUsers)]);
  });

  socket.on("disconnect", () => {
    activeUsers.delete(socket.userId);
    io.emit('userDisconnected', socket.userId);
  });

  socket.on('locationSubmitted', async function (data) {
    const playerRound = { lat: data['y'], lon: data['x'] } as PlayerRound
    const point = { lon: -91.37973583832658, lat: 30.68040748563647 } as Point
    const score = await submitResult(point, playerRound)
    console.log(data)
    console.log(score)
    socket.emit('submittedResult', { userId: data.userId, point, score })
    io.emit('resultReceived', { userId: data.userId, point, score })
  });

  socket.on("usePowerup", (data) => {
    console.log(data)
    io.emit('sendPowerup', { userId: data.userId, powerupId: data.powerupId })
  });

  socket.on("sendMessage", (data) => {
    console.log(data)
    io.emit('sendMessageToRoom', { userId: data.userId, msg: data.msg })
  });
});

export function start() {
  server.listen(3333);
  const timer = new BattleTimer('def')
  timer.runTimer()
}
