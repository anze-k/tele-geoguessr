import { RoundScorer } from "./Scoring";
import { Point, PlayerRound } from "./Types";

const roundScorer = new RoundScorer()

export function insertBattle() {}

export async function submitResult(point: Point, attempt: PlayerRound) {
    const score = await roundScorer.scoreRound(point, attempt)

    return score
}

export function getBattle() {}
export function finishBattle() {}
