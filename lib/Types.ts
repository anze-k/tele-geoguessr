export interface Player {
    id: string,
    tName?: string,
    gName?: string,
    email?: string,
    avatar?: string,
    elo?: string,
    lastRatingChange?: string,
    maxScore?: string,
    matchCount?: string,
    matchHistory?: PlayerChallenge[],
    rank?: string,
}

export interface Challenge {
    id: string,
    geoId: string,
    points?: string,
    type?: string,
}

export interface PlayerChallenge extends Challenge {
    playerId: string,
    score: string,
    ratingChange: string,
    fetchTime: string,
    elo: string,
    tName: string,
}

export interface PlayerRound {
    roundId: string,
    challengeId: string,
    playerId: string,
    score?: number,
    lat: string,
    lon: string,
    distance?: number,
    time: string,
    name?: string,
    type?: string,
}

export interface Area {
    gpsGenerator: () => any,
    namesInc: string[],
    namesExc: string[],
    radius: number,
    name: string,
}

export interface Point {
    country: string,
    short: string,
    lon: number,
    lat: number,
    type: string,
    id: string,
    challengeId?: string,
    time?: string,
    challengeType: string,
}

export interface Stats {
    country: string,
    distance: number,
	distanceRank: number,
	hit: number,
	hitRank: number,
	player: number,
	points: number,
    pointsRank: number,
    msg?: string,
}

export enum StatsCategory {
    distance = "distance",
    points = "points",
    hit = "hit",
}