const Telegraf = require('telegraf');
const AsciiTable = require('ascii-table');
const config = require('config');

import { Restrictor } from './Restrictor';
import { PlayerManager } from './PlayerManager';
import { getLogger } from './Logger';
import { fetchStats, generateStats } from './Stats';

const bot = new Telegraf(config.get('bot.token'));
const channelId = config.get('bot.channelId');
const log = getLogger('Botter');

export class Botter {
    constructor (private db, private challengeManager) {}
    private restrictor = new Restrictor(this.db);
    private playerManager = new PlayerManager(this.db);

    start() {
        bot.on('message', async (ctx) => {
            if (!ctx.message.text) return; // why is this even needed?
            // if (ctx.message.from.first_name !== "anže") {
            //   ctx.reply(`Not now!`);
            //   return
            // }
          //   if (ctx.message.text.startsWith('!a')) {
          //   if (!await this.restrictor.runSpamFilter(ctx.message.from.first_name)) return;

          //   const msg = ctx.message.text.split(' ');
          //   if (msg.length != 3) {
          //       // ctx.reply('Use moar parameters. Type !a [GeoGuessrNickname] [Email]');
          //       return;
          //   }

          //   try {
          //     await this.playerManager.addPlayer(ctx.message.from.first_name, msg[1], msg[2]);
          //     log.info(`Added new player: ${ctx.message.from.first_name}, ${msg[1]}, ${msg[2]}`);
          //   } catch (err) {
          //     log.error(err);
          //     // ctx.reply(`Sorry but is your geoguessr nick already in my database?`);
          //   }
          //  }

           if (ctx.message.text.startsWith('!c')) {
            if (!await this.restrictor.runSpamFilter(ctx.message.from.first_name)) return;
            // ctx.reply(`no`);
            // return;

            try {
              if (!await this.restrictor.doesPlayerExist(ctx.message.from.first_name)) {
                // ctx.reply(`I don't seem to know you. Use !a [GeoGuessrNickname] [Email] for that to change!`);
                return;
              }
          
              let type = 'World';
              if (ctx.message.text.split(' ')[1] === 'slo') {
                type = 'Slovenia'
              } else if (ctx.message.text.split(' ')[1] === 'ned') {
                type = 'Netherlands'
              } else if (ctx.message.text.split(' ')[1] === 'uk') {
                type = 'United Kingdom'
              } else if (ctx.message.text.split(' ')[1] === 'swe') {
                type = 'Sweden'
              }

              const params = ctx.message.text.split(' ');
              const geoId = await this.challengeManager.insertChallenge(type);
          
              ctx.reply(`🌎 New ${type} challenge awaits! [${geoId}](http://lckp.eu/${geoId}) 🌎`, Telegraf.Extra.markdown());
            } catch (err) {
                log.error(err);
                // ctx.reply(`Strange, I failed somehow. Try again later`);
            }
           }

           if (ctx.message.text.startsWith('!s')) {
            if (!await this.restrictor.runSpamFilter(ctx.message.from.first_name)) return;
            
            try {
              if (!await this.restrictor.doesPlayerExist(ctx.message.from.first_name)) {
                // ctx.reply(`I don't seem to know you. Use !a [GeoGuessrNickname] [Email] for that to change!`);
                return;
              }
          
              const params = ctx.message.text.split(' ');

              if (!params[1]) {
                if (ctx.message.from.first_name !== "anže") return;
                const generated = await generateStats(this.db);
                ctx.reply(generated, Telegraf.Extra.markdown());
                return;
              }

              let player;

              try {
                if (params[2]) { 
                  player = await this.playerManager.getPlayerByName(params[2]);
                } else {
                  player = await this.playerManager.getPlayerByName(ctx.message.from.first_name);
                }
              } catch (err) {
                ctx.reply(`Could not find player with name ${params[2]}`);
                return;
              }

              const playerId = player.id;
              const playerName = player.tName;
              
              const playerStats = await fetchStats(params[1], parseInt(playerId));

              if ('msg' in playerStats) {
                ctx.reply(playerStats['msg'], Telegraf.Extra.markdown());
                return;
              }
  
              const msg = `
              Player: *${playerName}* ${Math.random() >= 0.5 ? `👩‍ `: `👨‍ `}Country: ${playerStats['country']}
              average distance: *${playerStats['distance']}* km (${Botter.positionHelper(playerStats['distanceRank'])}),
              average points:  *${playerStats['points']}* pts (${Botter.positionHelper(playerStats['pointsRank'])}),
              average hit %:  *${playerStats['hit']}*% (${Botter.positionHelper(playerStats['hitRank'])})`;
              
              ctx.reply(msg, Telegraf.Extra.markdown());
              return;
            } catch (err) {
                log.error(err);
                ctx.reply(`Strange, I failed somehow. Try again later`);
            }
           }
          
           if (ctx.message.text.startsWith('!l')) {
            if (!await this.restrictor.runSpamFilter(ctx.message.from.first_name)) return;
          
            try {
              if (!await this.restrictor.doesPlayerExist(ctx.message.from.first_name)) {
                ctx.reply(`I don't seem to know you. Use !a [GeoGuessrNickname] [Email] for that to change!`);
                return;
              }
          
              const res = await this.playerManager.getHighScore();

              const table = new AsciiTable().setHeading('Name', 'Ranking', 'TopScore', 'Played');

              for (let i = 0; i < res.length; i++) {
                table.addRow(
                    `${res[i].tName}${i === 0 ?  '🥇' : ''}${i === 1 ?  '🥈' : ''}${i === 2 ?  '🥉' : ''}`,
                    `${res[i].elo} (${res[i].lastRatingChange ? res[i].lastRatingChange : '+0'})`,
                    res[i].maxScore,
                    res[i].matchCount);
              }

              const tb = '```'+table.toString()+'```';
              ctx.reply(tb, Telegraf.Extra.markdown());
          
            } catch (err) {
                log.error(err);
                ctx.reply(`Strange, I failed somehow. Try again later`);
            }
           }
          
           if (ctx.message.text.startsWith('!m')) {
            if (!await this.restrictor.runSpamFilter(ctx.message.from.first_name)) return;
          
            try {
              if (!await this.restrictor.doesPlayerExist(ctx.message.from.first_name)) {
                ctx.reply(`I don't seem to know you. Use !a [GeoGuessrNickname] [Email] for that to change!`);
                return;
              }
            const res = await this.playerManager.getUserInfo(ctx.message.from.first_name);

            const table1 = new AsciiTable().setHeading('Name', 'Ranking', 'Top score', 'Played');
            table1.addRow(`${Math.random() >= 0.5 ? `👩‍ `: `👨‍ `} ${res.tName}`,
                `${res.elo} (${res.lastRatingChange ? res.lastRatingChange : '+0'})`,
                res.maxScore,
                res.matchCount);
               
            const table2 = new AsciiTable()
            .setTitle('*Your last challenges:*');
            
            for (const match of res.matchHistory) {
                table2.addRow(' `🌎'+match.fetchTime+'`', 
                `[(+)](https://geoguessr.com/challenge/${match.geoId})`,
                match.score,
                `${match.ratingChange ? match.ratingChange : 'pending'}`);
            }

            const tb = '```'+table1.toString()+'```';
            const tb2 = table2.removeBorder().toString();
            ctx.reply(tb, Telegraf.Extra.markdown());
            ctx.reply(tb2, Telegraf.Extra.markdown());
            } catch (err) {
                log.error(err);
                ctx.reply(`Strange, I failed somehow. Try again later`);
            }
           }
          
           if (ctx.message.text.startsWith('!o')) {
            if (!await this.restrictor.runSpamFilter(ctx.message.from.first_name)) return;
          
            try {
              if (!await this.restrictor.doesPlayerExist(ctx.message.from.first_name)) {
                ctx.reply(`I don't seem to know you. Use !a GeoGuessrNickname Email for that to change!`);
                return;
              }
          
              const challenges = await this.challengeManager.fetchOpenChallenges(ctx.message.from.first_name);

              if (!challenges || challenges.length === 0) {
                ctx.reply(`You have no available challenges. Type !c to create one!`, Telegraf.Extra.markdown());
                return;
              }
              const table = new AsciiTable().setTitle(`*${ctx.message.from.first_name}*, your available open challenges:`);

              for (let i = 0; i < challenges.length; i++) {
                table.addRow(
                  `[${challenges[i].geoId}](http://lckp.eu/${challenges[i].geoId})`,
                    `${challenges[i].type}`,
                    `${challenges[i].scoreAfter}`);
              }

              const tb = table.removeBorder().toString();
              ctx.reply(tb, Telegraf.Extra.markdown());

            } catch (err) {
                log.error(err);
                ctx.reply(`Strange, I failed somehow. Try again later`);
            }
           }
          
           if (ctx.message.text.startsWith('!h')) {
            if (!await this.restrictor.runSpamFilter(ctx.message.from.first_name)) return;
          
            try {
              if (!await this.restrictor.doesPlayerExist(ctx.message.from.first_name)) {
                ctx.reply(`I don't seem to know you. Use !a GeoGuessrNickname Email for that to change!`);
                return;
              }
             
              let html = '';
              html += `🌎 *Commands* 🌎 \n`;
              html += `\n`;
              html += `_!a geoNick email_ - adds you to the bot so it can follow your scores \n`;
              html += `_!m_ - displays your stats and match history \n`;
              html += `_!c_ - creates a new geo challenge. can take up to 10 seconds \n`;
              html += `_!c number_ - creates a new geo challenge that will be scored in [number] minutes \n`;
              html += `_!l_ - lists current ladder standings \n`;
              html += `_!o_ - lists ongoing challenges which you can play \n`;
              html += `_!h_ - displays help \n`;
              html += `\n`;
              html += `The commands work in private messages as well as the channel.`;
              
              ctx.reply(html, Telegraf.Extra.markdown());
          
            } catch (err) {
                log.error(err);
                ctx.reply(`Strange, I failed somehow. Try again later`);
            }
           }
          });

          bot.startPolling();
          log.info('Bot started!');
    }

    static sendMessage(msg: string) {
      try {
        bot.telegram.sendMessage(channelId, msg, Telegraf.Extra.markdown());
      } catch (err) {
        log.warn(err, `Bot failed to send message`);
      } 
    }

    static positionHelper(position: number) {
      switch (position) {
        case 1:
          return '🥇';
          break;
        case 2:
          return '🥈';
          break;
        case 3:
          return '🥉';
          break;
        default:
          return position+"th";
      }
    }
}