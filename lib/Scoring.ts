const EloRating = require('elo-rating');
const config = require('config');
const distance = require('gps-distance');

import { getLogger } from './Logger';
import { Botter } from './Botter';
import { RoundManager } from './RoundManager';
import { PlayerManager } from './PlayerManager';
import { PlayerChallenge, Point, PlayerRound } from './Types';

const scoringLoopTime: number = config.get('flow.checkForFinishedChallenges');
const log = getLogger("Scorer");

export class Scorer {
    constructor (private db) {}
    private roundManager = new RoundManager(this.db);
    private playerManager = new PlayerManager(this.db);

    start() {
        this.processScoring();
        setInterval(this.processScoring.bind(this), scoringLoopTime);
    }

    scoreImmediately () {
        return this.processScoring();
    }

    private async processScoring () {
        const challengesToScore = await this.db.prepare(`SELECT geoId from challenges
            where scoredAt is NULL and scoreAfter < datetime('now') `)
            .all();

        if (!challengesToScore || challengesToScore.length === 0) return;

        const geoIds = challengesToScore.map(chall => {
            return chall.geoId;
        });

        const geoId = geoIds[0];

        // conclude non finishing players
        const participants = await this.db.prepare(`
            select count(*) as cunt, playerId from playerRounds 
            where challengeId = ? group by playerId`).all(geoId);
        
        for (const participant of participants) {
            try {
                if (parseInt(participant.cunt) == 5) continue;
                const realChallengeId = await this.db.prepare(`SELECT id from challenges  where geoId = ?`).get(geoId).id;
                const rounds = await this.roundManager.getRoundsInfoForPlayer(participant.playerId, geoId);
                const totalScore = rounds.reduce((res, round) => res + round.score, 0 );
                await this.playerManager.insertScore(realChallengeId, participant.playerId, geoId, totalScore.toString());
            } catch (ex) {
                log.error(`Could not score non-finished participant ${participant.playerId} on challenge ${geoId}`);
                log.error(ex);
            }
        }
        // concluded, now score

        const playerChallenges = await this.db.prepare(`
            SELECT ps.playerId, ps.geoId, ps.score,
            ps.ratingChange, ps.fetchTime, p.elo, p.tName, c.id from playerScores ps
            inner join players p on ps.playerId = p.id
            inner join challenges c on c.geoId = ps.geoId
            where ps.geoId IN (${geoId})`)
            .all() as PlayerChallenge[];

        log.info(`Found ${geoIds.length} challenges to be scored`);
        const filteredPCs = playerChallenges.filter(pc => pc.geoId === geoId);
        await this.score(filteredPCs, geoId);
    }

    private async score(players: PlayerChallenge[], geoId) {
        let newRatings: number[] = [];
        let ratingDifferences: number[] = [];

        if (!players || players.length === 0) {
            log.info(`No players for ${geoId}, concluding`);
            await this.db.prepare(`UPDATE challenges set scoredAt = CURRENT_TIMESTAMP WHERE geoId = ?`).run(geoId);
            return;
        }

        if (players.length === 1) {
            log.info(`Only 1 player for ${geoId} (${players[0].tName}), concluding`);
            await this.db.prepare(`UPDATE challenges set scoredAt = CURRENT_TIMESTAMP WHERE geoId = ?`).run(geoId);
            return;
        } else {
            for (let i = 0; i < players.length; i++) {
                const currentElo = parseInt(players[i].elo);
                let ratingDiff: number = 0;
    
                for (let j = 0; j < players.length; j++) {
                    if (i === j) continue;
    
                    ratingDiff += EloRating.calculate(currentElo, parseInt(players[j].elo), 
                    players[i].score > players[j].score, 40).playerRating - currentElo;
                }
                
                ratingDiff = Math.round(ratingDiff/(players.length - 1));

                const newRating = ratingDiff + currentElo;
                newRatings.push(newRating);
                ratingDifferences.push(ratingDiff);
            }
    
            for (let i = 0; i < players.length; i++) {
                await this.db.prepare(`UPDATE playerScores set ratingChange = ? WHERE challengeId = ? AND playerId = ?`)
                .run(ratingDifferences[i], players[i].id, players[i].playerId);
                await this.db.prepare(`UPDATE players set elo = ? WHERE id = ?`)
                .run(newRatings[i], players[i].playerId);
            }
        }    

        const scoredPlayerNames = players.map(player => player.tName);
        const oldRankings = players.map(player => player.elo);

        await this.db.prepare(`UPDATE challenges set scoredAt = CURRENT_TIMESTAMP WHERE geoId = ?`).run(players[0].geoId);
        log.info(`Scored players: ${players.map(player => player.tName)}`);
        log.info(`Old ratings: ${players.map(player => player.elo)} New ratings: ${newRatings.join(',')}`);

        // Scoring is now complete. msg the channel?
        let msg = `[${players[0].geoId}](http://lckp.eu/${players[0].geoId}) scoring complete! \n`;
        for (let i = 0; i < players.length; i++) {
            msg += `*${scoredPlayerNames[i]}*  - Old rating: ${oldRankings[i]}, New rating *${newRatings[i]}*\n`
        }

        Botter.sendMessage(msg);
        
        return(newRatings);
    }
}

export class RoundScorer {
    constructor () {}

    scoreRound(gpsPoint: Point, playerAttempt: PlayerRound) {
        let constantDiv = 2000
        if (playerAttempt.type === 'Slovenia') {
            constantDiv = 46
        } else if (playerAttempt.type === 'Netherlands') {
            constantDiv = 70
        } else if (playerAttempt.type === 'United Kingdom') {
            constantDiv = 550
        } else if (playerAttempt.type === 'Sweden') {
            constantDiv = 700
        }


        playerAttempt.distance =
            parseFloat(distance(gpsPoint.lat, gpsPoint.lon, playerAttempt.lat, playerAttempt.lon));

        playerAttempt.score = Math.round(5000 * Math.pow(Math.E, - (playerAttempt.distance) / constantDiv));

        return playerAttempt
    }
}


