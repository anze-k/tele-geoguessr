const bunyan = require('bunyan');
const bformat = require('bunyan-format');

const formatOut = bformat({ outputMode: 'long'}) 

export function getLogger(className: string) {
    return bunyan.createLogger({name: className, stream: formatOut});
}
