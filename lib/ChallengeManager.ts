const puppeteer = require('puppeteer');
const config = require('config');

import { Coordinator } from './Coordinator';
import { Challenge } from './Types';
import { getLogger } from './Logger';

const challengeLoopTime = config.get('flow.fetchChallengeScoreInterval');

const log = getLogger('ChallengeManager');

export class ChallengeManager {
    constructor (private db) {}

    private coordinator = new Coordinator(this.db);

    insertChallenge = async (type: string, pointsNum = 5, scoreAfter: string = '2880') => {
        try {
            const points = await this.coordinator.getUnusedPoints(type);
            const pointIds = points.map(point => point.id).slice(0, pointsNum);
            const geoId = Date.now();

            await this.db.prepare(`INSERT INTO challenges (geoId, type, scoreAfter, points)
                VALUES (cast(? as INTEGER), ?, datetime('now','+${scoreAfter} minutes'), ?);`).run(geoId, type, pointIds.toString());

            for (let point of pointIds) {
                await this.db.prepare(`update points set challengeId = CAST(? as INTEGER) where id = ?`).run(geoId, point);
            }

            return geoId;
        } catch (err) {
            log.error(err, 'Error inserting a challenge');
        }
    }

    getChallengeHighScore = async (geoId: string) => {
        const players = await this.db.prepare(`select ps.geoId, ps.score, p.gName from playerScores ps 
            inner join players p on ps.playerId = p.id where geoId = ? order by ps.score DESC`).all(geoId);
        
        players.forEach((element, i) => {
            element.position = i + 1;
        });

        return players;
    }

    fetchOpenChallenges = async (tName: string) => {
        const id = await this.db.prepare(`
        select id from players where tName = ?
        `).get(tName);

        const challenges = await this.db.prepare(`
        select * from challenges as c WHERE c.scoredAt IS NULL and NOT EXISTS 
        (SELECT * FROM playerScores as sc
        WHERE sc.playerId = ? and challengeId = c.id)
        `).all(id.id);
        
       return challenges;
    }

    fetchPendingChallenges = async (tName: string) => {
        const id = await this.db.prepare(`
        select id from players where tName = ?
        `).get(tName);

        const challenges = await this.db.prepare(`
        select * from challenges as c WHERE c.scoredAt IS NULL and EXISTS 
        (SELECT * FROM playerScores as sc
        WHERE sc.playerId = ? and challengeId = c.id)
        `).all(id.id);
        
       return challenges;
    }

    fetchLastRated = async (tName: string) => {
        const id = await this.db.prepare(`
        select id from players where tName = ?
        `).get(tName);

        const challenges = await this.db.prepare(`
        select c.type, c.geoId, sc.ratingChange from challenges as c JOIN playerScores as sc on sc.playerId = ? 
        and challengeId = c.id  WHERE c.scoredAt IS NOT NULL ORDER BY c.scoredAt DESC LIMIT 5
        `).all(id.id);
        
       return challenges;
    }

    getChallenge = async (geoId: string) => {
        const challenge = await this.db.prepare(`select * from challenges where geoId = ?`).get(geoId);
        return challenge as Challenge
    }
}

