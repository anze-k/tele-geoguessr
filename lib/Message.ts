import { Message } from "./Types";
const Database = require('better-sqlite3');
const db = new Database('tg.db', {});

export async function fetchLastMessages() {
    return await db.prepare(`select * from messages order by time desc limit 15`).all().reverse() as Message[];
}

export async function storeMessage(msgText: string, author: string) {
    await db.prepare(`INSERT INTO messages (message, author)
                VALUES (?, ?)`).run(msgText, author);
}