import { Scorer } from '../lib/Scoring';
import 'mocha';
import expect = require('expect');

const Database = require('better-sqlite3');


describe('scoring', function() {
    const db = new Database('test.db', {});

    before(async () => {
        const createScaffold = async () => {
            await db.exec(`CREATE TABLE IF NOT EXISTS players (id INTEGER PRIMARY KEY ASC, tName TEXT, gName TEXT UNIQUE, email TEXT, elo INTEGER DEFAULT 1500);`);
            await db.exec(`CREATE TABLE IF NOT EXISTS challenges (id INTEGER PRIMARY KEY ASC, geoId TEXT UNIQUE, type TEXT, addedTime DEFAULT CURRENT_TIMESTAMP,
              scoreAfter, scoredAt);`);
            await db.exec(`CREATE TABLE IF NOT EXISTS playerScores (challengeId INTEGER NOT NULL REFERENCES challenges(id), playerId INTEGER NOT NULL REFERENCES players(id),
             geoId TEXT, score INTEGER, fetchTime DEFAULT CURRENT_TIMESTAMP, ratingChange NUMBER);`);

             await db.exec(`DELETE FROM playerScores`);
             await db.exec(`DELETE FROM challenges`);
             await db.exec(`DELETE FROM players`);
          }
          
          await createScaffold();
      });

     it(`do scoring properly`, async () => {
        await db.exec(`insert into challenges (id, geoId, type, scoreAfter) values (1, 'fakegeoId', 'world', CURRENT_TIMESTAMP)`);
        await db.exec(`insert into players (tName, gName, email) values ('anze1', 'anze1', 'anze1')`);
        await db.exec(`insert into players (tName, gName, email) values ('anze2', 'anze2', 'anze2')`);
        await db.exec(`insert into players (tName, gName, email) values ('anze3', 'anze3', 'anze3')`);
        await db.exec(`insert into players (tName, gName, email) values ('anze4', 'anze4', 'anze4')`);
        await db.exec(`insert into players (tName, gName, email) values ('anze5', 'anze5', 'anze5')`);
        await db.exec(`insert into players (tName, gName, email) values ('anze6', 'anze6', 'anze6')`);
        await db.exec(`insert into playerScores (challengeId, geoId, playerId, score ) values (1, 'fakegeoId', 1, 10000)`);
        await db.exec(`insert into playerScores (challengeId, geoId, playerId, score ) values (1, 'fakegeoId', 2, 8000)`);
        await db.exec(`insert into playerScores (challengeId, geoId, playerId, score ) values (1, 'fakegeoId', 3, 6000)`);
        await db.exec(`insert into playerScores (challengeId, geoId, playerId, score ) values (1, 'fakegeoId', 4, 4000)`);
        await db.exec(`insert into playerScores (challengeId, geoId, playerId, score ) values (1, 'fakegeoId', 5, 2000)`);
        await db.exec(`insert into playerScores (challengeId, geoId, playerId, score ) values (1, 'fakegeoId', 6, 0)`);

        await db.exec(`insert into challenges (id, geoId, type, scoreAfter) values (2, 'fakegeoId2', 'world', CURRENT_TIMESTAMP)`);
        await db.exec(`insert into players (tName, gName, email) values ('anze7', 'anze7', 'anze7')`);
        await db.exec(`insert into players (tName, gName, email) values ('anze8', 'anze8', 'anze8')`);
        await db.exec(`insert into players (tName, gName, email) values ('anze9', 'anze9', 'anze9')`);
        await db.exec(`insert into playerScores (challengeId, geoId, playerId, score ) values (2, 'fakegeoId2', 7, 10000)`);
        await db.exec(`insert into playerScores (challengeId, geoId, playerId, score ) values (2, 'fakegeoId2', 8, 8000)`);
        await db.exec(`insert into playerScores (challengeId, geoId, playerId, score ) values (2, 'fakegeoId2', 9, 6000)`);


        await new Promise(resolve => setTimeout(resolve, 3000));

        const scorer = new Scorer(db);
        const scores = await scorer.scoreImmediately();
        expect(scores[0][0]).toEqual(scores[1][0]);

        
     }).timeout(5000);

    //  after(async () => {
    //     // const db.
    //  })
})


  