const Database = require('better-sqlite3')
require('./web.ts')
import { ChallengeManager } from './lib/ChallengeManager'
import { Scorer } from './lib/Scoring'
import { Botter } from './lib/Botter'
import { Coordinator } from './lib/Coordinator'
import { PlayerManager } from './lib/PlayerManager'
import { RoundManager } from './lib/RoundManager'
import { start as startSocketServer } from './lib/sockets/server'

const db = new Database('tg.db', {});

const createScaffold = async () => {
  await db.exec(`CREATE TABLE IF NOT EXISTS players (id INTEGER PRIMARY KEY ASC, tName TEXT, gName TEXT UNIQUE, email TEXT, elo INTEGER DEFAULT 1500);`);
  await db.exec(`CREATE TABLE IF NOT EXISTS challenges (id INTEGER PRIMARY KEY ASC, geoId TEXT UNIQUE, type TEXT, addedTime DEFAULT CURRENT_TIMESTAMP,
    scoreAfter, scoredAt, points);`);
  await db.exec(`CREATE TABLE IF NOT EXISTS playerScores (challengeId INTEGER NOT NULL REFERENCES challenges(id), playerId INTEGER NOT NULL REFERENCES players(id),
  geoId TEXT, score INTEGER, fetchTime DEFAULT CURRENT_TIMESTAMP, ratingChange NUMBER);`);
  await db.exec(`CREATE TABLE IF NOT EXISTS points (id INTEGER PRIMARY KEY ASC, challengeId INTEGER REFERENCES challenges(geoId), lon Decimal(3,8), lat Decimal(3,8),
   country text, type text, googleId text, used boolean default false, time DEFAULT CURRENT_TIMESTAMP);`);
  await db.exec(`CREATE TABLE IF NOT EXISTS playerRounds (roundId integer, challengeId integer, playerId integer, score integer, lon, lat, distance, time DEFAULT CURRENT_TIMESTAMP,  UNIQUE(challengeId, playerId, roundId));`);
  
  try {
    await db.exec(`alter table challenges add points`);
  } catch (e) {}

  try {
    await db.exec(`alter table points add short`);
  } catch (e) {}

  try {
    await db.exec(`alter table points add challengeType`);
  } catch (e) {}

  try {
    await db.exec(`alter table players add avatar`);
  } catch (e) {}
  
  // await db.exec(`update table players add column tId, avatar`);
}

createScaffold().then(() => {
  const challengeManager = new ChallengeManager(db);
  const scorer = new Scorer(db);
  const botter = new Botter(db, challengeManager);
  const coordinator = new Coordinator(db);
  coordinator.start();
  // challengeManager.start();
  scorer.start();
  botter.start();
  startSocketServer()
});

