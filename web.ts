var express = require("express");
var bodyParser = require("body-parser");
const csp = require('express-csp-header');
var routes = require("./web/rest.ts");
var index = require("./web/index.js");
var app = express();
const cookieParser = require('cookie-parser');

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('./web/assets'));
app.use(express.static('./web/assets/flags'));


routes(app);
index(app);

var server = app.listen(3000, function() {
    console.log("app running on port.", server.address().port);
});