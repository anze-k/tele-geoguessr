const Database = require('better-sqlite3');
const db = new Database('./tg.db', {});

import { RoundManager } from '../lib/RoundManager';
import { PlayerManager } from '../lib/PlayerManager';
import { ChallengeManager } from '../lib/ChallengeManager';
import { topStatsPerPlayer } from '../lib/Stats';
import { Player } from '../lib/Types';

const roundManager = new RoundManager(db);
const playerManager = new PlayerManager(db);
const challengeManager = new ChallengeManager(db);

const challengeRoute = '/rest/userChallenge';
const userRoute = '/rest/user';

var rest = function(app) {
    app.get(`${challengeRoute}/:challengeId`, async function(req, res) {
        const points = await roundManager.getPointsForChallenge(req.params.challengeId);
        if (!points || points.length == 0) {
            res.status(500).send('No such challenge');
            return;
        }

        console.log(req.query.user)
        const player = await playerManager.getPlayerByName(req.query.user.first_name)
        if (!player) {
            res.status(500).send('Cannot find player');
            return
        }

        const allPlayerRounds = await roundManager.getRoundsInfo(req.params.challengeId);
        const playerRounds = allPlayerRounds.filter(round => round.playerId == player.id);
  
        let currentRound = playerRounds.length + 1

        const currentScore = playerRounds.reduce((res, round) => res + round.score, 0 );
        let currentHighscore = {}
        
        const filteredPlayerRounds = allPlayerRounds.filter(round => parseInt(round.roundId) < currentRound)
        for (let round of filteredPlayerRounds) {
            const player = await playerManager.getPlayerById(round.playerId);
            round.name = player.tName.replace('^', '');
            round['rank'] = player.rank;
        }

        for (let playerRound of filteredPlayerRounds) {
            const noSpaceName = playerRound.name.replace(" ", "").replace('^', '').replace("}", ""). replace("{", "")
            if (currentHighscore[noSpaceName])
                currentHighscore[noSpaceName].score += playerRound.score
            else {
                currentHighscore[noSpaceName] = {}
                currentHighscore[noSpaceName].score = playerRound.score           
                currentHighscore[noSpaceName].rank = playerRound['rank'];
            }

            if (parseInt(playerRound.roundId) === currentRound - 2) {
                currentHighscore[noSpaceName].lastScore = playerRound.score;
            }

            if (parseInt(playerRound.roundId) === currentRound - 1) {
                currentHighscore[noSpaceName].lat = playerRound.lat;  
                currentHighscore[noSpaceName].lon = playerRound.lon;  
            }
        }

        let sortedKeys = Object.keys(currentHighscore).sort(function(a,b){return currentHighscore[b].score-currentHighscore[a].score})
        const finalHighscore = {}
        for (let key of sortedKeys) {
            
            finalHighscore[key] = currentHighscore[key];
        }

        // first round! all we need is the first point... i think
        if (currentRound == 1) {
            res.status(200).send({ 
                points: [ points[0] ],
                currentRound,
                currentScore,
                currentHighscore: finalHighscore,
            });
        } else {
            res.status(200).send({ 
                points: points.slice(0, currentRound),
                rounds: playerRounds,
                currentRound,
                currentScore,
                currentHighscore: finalHighscore,
                filteredPlayerRounds
            });
        }
    });

    app.post(`${challengeRoute}/postResult`, async function(req, res) {
        const { roundId, challengeId, first_name, lat, lon } = req.body;
        const player = await playerManager.getPlayerByName(first_name)
        if (!player) res.status(500).send('Cannot find player');


        res.status(200).send(await roundManager.postResult(roundId, challengeId, player.id, lat, lon));
    });

    app.post(`${userRoute}/getUser`, async function(req, res) {
        const {id, first_name, photo_url, auth_date, hash } = req.body;
        let player = await playerManager.getPlayerByName(first_name)
        if (!player) {
            await playerManager.addPlayer(first_name, first_name, "", photo_url)
            player = await playerManager.getPlayerByName(first_name)
        } else if (!player.avatar) {
            await playerManager.updateAvatar(first_name, photo_url)
            player = await playerManager.getPlayerByName(first_name)
        }

        res.status(200).send(player);
    });

    app.get(`${userRoute}/dashboard`, async function(req, res) {
        const userName = req.query.username;

        const user: Player = await playerManager.getPlayerByName(userName)

        const openChallenges = await challengeManager.fetchOpenChallenges(userName)
        const pendingChallenges = await challengeManager.fetchPendingChallenges(userName)
        const ratedChallenges = await challengeManager.fetchLastRated(userName)

        const highscore = await playerManager.getHighScore()

        const challenges = await db.prepare(`select c.geoId, c.type, c.scoreAfter from challenges as c 
        WHERE c.scoredAt IS NULL group by c.geoId order by c.addedTime DESC`).all();

        const stats = await topStatsPerPlayer(user.id)

        res.send({ 
            challenges: {
                open: openChallenges,
                pending: pendingChallenges,
                rated: ratedChallenges,
            },
            stats,
            highscore
        })
    });

    app.post(`/rest/challenge/create`, async function(req, res) {
        console.log(req.body)
        const geoId = await challengeManager.insertChallenge(req.body.type)

        res.sendStatus(200);
    });

}

module.exports = rest;