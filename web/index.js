const Database = require('better-sqlite3');
const db = new Database('./tg.db', {});
const config = require('config');
const axios = require('axios');
const PlayerManager = require('../lib/PlayerManager').PlayerManager;

const CLIENT_SECRET = config.get('discord.clientSecret');
const playerManager = new PlayerManager(db);

const index = function(app) {
    app.get('/point*', function(req, res) {
        res.sendFile(__dirname + '/point.html');
    });

    app.get('/*.result', function(req, res) {
        res.sendFile(__dirname + '/result.html');
    });

    app.get('/list*', async function(req, res) {
        const challenges = await db.prepare(`select c.geoId, c.type, c.scoreAfter from challenges as c 
            WHERE c.scoredAt IS NULL group by c.geoId order by c.addedTime DESC`).all();

        let list = ``
        for (let chall of challenges) {
            const player = await db.prepare(`select count(distinct playerId) as count from playerRounds where challengeId = ?`).get(chall.geoId);
            list += `<a href='http://lckp.eu/${chall.geoId}'><b>${chall.type}</b> - ${chall.scoreAfter} (${player.count}) active players)<br />`
        }

        list += `<br /><br /> <a href='http://lckp.eu/rest/challenge/create'><b>Create new challenge</b>`
        res.send(list)
    });


    app.get('/discord*', async function(req, res, next) {
        if (req.query.code) {
            const payload = new URLSearchParams()

            payload.append('client_id', '798950451106152488')
            payload.append('client_secret', CLIENT_SECRET)
            payload.append('grant_type', 'authorization_code')
            payload.append('redirect_uri', 'http://lckp.eu/discord')
            payload.append('code', req.query.code)
            payload.append('scope', 'identify')

            axios.post('https://discord.com/api/oauth2/token', payload,
                    headers = {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    })
                .then((response) => {
                    console.log(response)
                    if (response.data.access_token) {
                        axios.get('https://discord.com/api/oauth2/@me', { headers: { 'Authorization': `Bearer ${response.data.access_token}` } }).then(async(r, resp) => {
                            const username = r.data.user.username
                            console.log(r.data.user, username)
                            const player = await playerManager.getPlayerByName(username)
                            if (!player) {
                                await playerManager.addPlayer(username, username, username, r.data.user.avatar)
                            }

                            first_name = username
                            res.cookie('first_name', first_name);
                            res.cookie('avatar', '');
                            res.redirect('/');
                            next()

                        }, (error) => {
                            console.log(error);
                            resp.status(500).send('Errord. sorry');
                        });
                    }

                }, (error) => {
                    console.log(error);
                    res.status(500).send('Errord. sorry');
                });
        } else {
            console.log('no query code')
        }
    })

    app.get('/battle', function(req, res) {
        res.sendFile(__dirname + '/battle.html');
    });

    app.get('/battle2', function(req, res) {
        res.sendFile(__dirname + '/battle2.html');
    });

    app.get('/([0-9]*$)', function(req, res) {
        res.sendFile(__dirname + '/index.html');
    });

    app.get('/error', function(req, res) {
        res.status(500).send('Errord. sorry');
    });

    app.get('/', async function(req, res) {
        res.sendFile(__dirname + '/temp.html');
    });
}

module.exports = index;